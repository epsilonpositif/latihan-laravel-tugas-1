@extends('layout.master')
@section('judul')
    Data Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary my-4">Tambahkan cast</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=> $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{ $item->bio }}</td>
                    <td>
                        
                        <form action="/cast/{{$item->id}}" method="post">
                            <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <button class="btn btn-danger btn-sm" type="submit" name="delete">Hapus</button>
                        </form>
                    </td>

                </tr>

            @empty
                <h2>Data tidak tersedia</h2>
            @endforelse
        </tbody>
    </table>
@endsection
