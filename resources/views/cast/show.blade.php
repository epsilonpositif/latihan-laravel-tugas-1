@extends('layout.master')
@section('judul')
Detail Cast
@endsection

@section('content')
    <h2>{{$cast->nama}}</h2>
    <hr>
    <div>Umur : {{$cast->umur}}</div>
    <div>Bio : {{$cast->bio}}</div>
    <a href="/cast" class="btn btn-info my-4">Kembali</a>
@endsection