@extends('layout.master')
@section('judul')
Tambah Cast
@endsection

@section('content')
<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" class="form-control" name="nama" id="exampleInputEmail1" aria-describedby="emailHelp">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Umur</label>
        <input type="text" class="form-control" name="umur" id="exampleInputPassword1">
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <input type="text" class="form-control" name="bio" id="exampleInputPassword1">
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <a href="/cast" class="btn btn-danger">Kembali</a>
    <button type="submit" class="btn btn-primary">Submit</button>
    
</form>
@endsection