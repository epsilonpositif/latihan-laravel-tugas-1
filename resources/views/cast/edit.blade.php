@extends('layout.master')
@section('judul')
    Edit Cast
@endsection

@section('content')
    <form method="POST" action="/cast/{{$cast->id}}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" class="form-control" name="nama" id="exampleInputEmail1" value="{{$cast->nama}}" aria-describedby="emailHelp">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Umur</label>
            <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="exampleInputPassword1">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Bio</label>
            <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="exampleInputPassword1">
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <a href="/cast" class="btn btn-danger">Kembali</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
