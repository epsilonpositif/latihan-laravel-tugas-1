@extends('layout.master')
@section('judul')
Buat Account Baru
@endsection

@section('content')
<h3>Sign Up Form</h3>
    <form method="POST" action="/welcome">
        @csrf
        <label for="txt-first">First name</label><br /><br />
        <input type="text" name="first" id="txt-first" /><br /><br />
        <label for="txt-last">Last name</label><br /><br />
        <input type="text" name="last" id="txt-last" /><br /><br />
        <label>Gender</label><br /><br />
        <input type="radio" name="gender" value="male" />Male <br />
        <input type="radio" name="gender" value="female" />Female <br /><br />

        <label>Nationality</label><br /><br />
        <select name="lang" id="lang">
            <option value="bhs">Bahasa Indonesia</option>
            <option value="eng">English</option>
            <option value="lain">Other</option>
        </select><br /><br />
        <label>Language spoken</label><br /><br />
        <input type="checkbox" name="bhs" /><label>Bahasa Indonesia</label><br />
        <input type="checkbox" name="eng" /><label>English</label><br />
        <input type="checkbox" name="other" /><label>Other</label><br /><br />

        <label>Bio</label><br /><br />
        <textarea name="bio" id="" cols="30" rows="10"> </textarea><br />
        <button type="submit">Sign Up</button>
    </form>
@endsection




