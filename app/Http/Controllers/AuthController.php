<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('/halaman/form');
    }

    public function welcome(Request $request){
        $nama = $request['first'];
        $nama2 = $request['last'];
        return view('/halaman/welcome', compact('nama','nama2'));
        // dd($request->all());
    }
}
