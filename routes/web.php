<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'indexController@index');
Route::get('/data-tables', 'IndexController@table' );
Route::get('/register', 'AuthController@form' );

Route::post('/welcome', 'AuthController@welcome' );
Route::get('/master', function(){
    return view("layout/master");
} );

Route::get('/cast/create', 'CastController@create' ); //route untuk menampilkan form
Route::post('/cast', 'CastController@store' ); //route untuk menyimpan data

Route::get('/cast', 'CastController@index' ); 

Route::get('/cast/{cast_id}', 'CastController@show' );


Route::get('/cast/{cast_id}/edit', 'CastController@edit' );
Route::put('/cast/{cast_id}', 'CastController@update' );


Route::delete('/cast/{cast_id}', 'CastController@destroy' );







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
